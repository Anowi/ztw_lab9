package com.company;

public class Main {

    public Main(){
        System.out.println("Dziala konstruktor klasy Main");
    }

    private Long getFactorial(final long digit) {
        long wynik = 1;
        if (digit < 0) {
            return null;
        } else {
            for (int i = 2; i <= digit; i++) {
                wynik *= i;
            }
            return wynik;
        }
    }

    public static void main(String[] args) {
        long digit = 5;
        Main main = new Main() ;
        long factorial = main.getFactorial(digit);
        System.out.printf("Silnia(%d) = %d \n", digit, factorial);
    }
}
